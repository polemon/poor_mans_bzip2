#!/usr/bin/env python3
"""
    poor_mans_bzip2.py - bzip2 functions for BWT / MTF / RLE / Huffman encoding

    Author:      Symon 'polemon' Bereziak <polemon@polemon.org>
    Version:     0.01
    Last Change: 2016-08-01

"""

import os
import sys
import re
import operator
import pprint as pp

__NUL = u'\u2400' # special '0' for RLE
__SOH = u'\u2401' # special '1' for RLE
__STX = u'\u2402' # START OF TEXT

def bwt(msg):
    bwt_vec = list()
    # add begin delimiter of characters
    bwt_str = __STX + msg

    # create rotated permuations of string
    # first rotation
    bwt_vec.append(bwt_str)
    bwt_str = bwt_str[1:] + bwt_str[0]

    while(bwt_str[0] != __STX):
        bwt_vec.append(bwt_str)
        bwt_str = bwt_str[1:] + bwt_str[0]

    # lexicographically sort table
    bwt_vec.sort()

    # output string is last characters in each row
    bwt_out = ""
    for s in bwt_vec:
        bwt_out = bwt_out + s[-1]

    return bwt_out

def bwt_inv(bwt_str):
    bwt_vec = list()

    # build BWT decoder table
    for i in range(0, len(bwt_str)):
        bwt_vec.append('')

    # prepend each row with characters from BWT string and sort each time
    for i in range(0, len(bwt_str)):
        for j in range(0, len(bwt_str)):
            bwt_vec[j] = bwt_str[j] + bwt_vec[j]
        bwt_vec.sort()

    # string beginning with start delimiter is the decoded string
    for e in bwt_vec:
        if e[0] == __STX:
            return e[1:]

def mtf(bwt_str):
    mtf_lst = list()
    seq = list()

    # build dictionary list
    for i in bwt_str:
        if i not in mtf_lst:
            mtf_lst.append(str(i))
    mtf_lst.sort()
    mtf_lst = ''.join(mtf_lst)

    # put numbers in sequence, adjust list if neccesary
    for i in bwt_str:
        seq.append(mtf_lst.index(i))
        if mtf_lst.index(i) > 0:
            mtf_lst = mtf_lst[ mtf_lst.index(i) ] \
                    + mtf_lst[ : mtf_lst.index(i) ] \
                    + mtf_lst[ mtf_lst.index(i)+1 : ]

    mtf_lst = ''.join(sorted(mtf_lst))
    return [mtf_lst, seq]

def mtf_inv(mtf_o):
    mtf_lst = mtf_o[0]
    seq = mtf_o[1]
   
    # build string from sequence, permutate dictionary
    bwt_str = ""
    for i in seq:
        bwt_str = bwt_str + mtf_lst[i]
        if i > 0:
            mtf_lst = mtf_lst[ i ] \
                    + mtf_lst[ : i ] \
                    + mtf_lst[ i+1 : ]

    return bwt_str

def __to_bin(n):
    binlist = list()
    # force binary string representation
    b = str(bin(n))
    # remove '0b1' from each string
    b = b[3:]

    # transmogrify into "special" '0's and '1's
    for c in b:
        if c == '0':
            binlist.append(__NUL)
        else:
            binlist.append(__SOH)

    return binlist

def rle(seq):
    rleseq = list()

    cur = None
    n = 1
    for i in seq:
        if cur == i:
            # same character as before
            n += 1
        else:
            # different character in sequence
            if n > 1:
                # accumulated streak of same symbols
                rleseq += __to_bin(n)
            rleseq.append(i)
            n = 1
        cur = i

    return rleseq

def rld(rleseq):
    seq = list()

    b_num = '0b1'
    cur = None
    for i in rleseq:
        # decode special chars into bin number
        if i == __NUL:
            b_num += '0'
        elif i == __SOH:
            b_num += '1'
        else:
            # next "normal" char: append reppetitions, append current and clean up
            if b_num != '0b1':
                for j in range(int(b_num, 2)-1):
                    seq.append(cur)

            seq.append(i)
            b_num = '0b1'
            cur = i
    
    return seq

def __subtree(path, tree):
    if len(tree) > 2:
        __subtree(path + '0', tree[1]) # left branch
        __subtree(path + '1', tree[2]) # right branch
    else:
        print(str(tree[1]) + ": " + path)

    return tree

def huffman(rleseq):
    #this shit isn't even really planned, yet
    huff_d = dict()
    huff_t = list()

    # accumulate occurances
    for i in rleseq:
        if i in huff_d:
            huff_d[i] += 1
        else:
            huff_d[i] = 1

    for i in huff_d:
        # NOTE appending a tuple! (reverse logic dictionary)
        huff_t.append( (huff_d[i], i) )

    # build Huffman tree
    while len(huff_t) > 1:
        # sort elements by occurance
        huff_t = sorted(huff_t, key = lambda e: e[0])
        
        left = huff_t.pop(0)
        right = huff_t.pop(0)

        # push merged sub-branch back onto the tree
        huff_t.append( (left[0] + right[0], left, right) )

    huff_d.clear() # recycling is good for this planet!
    
    __subtree('', huff_t[0])

    pp.pprint(huff_t)


    #for i in sorted_x:
    #    print(sorted_x[0])

    bits = ''
    return [huff_d, bits]

def main():
    if len(sys.argv) <= 1:
        sys.exit(1)

    # encoding
    s_bwt = bwt(sys.argv[1])
    [mtf_dict, s_mtf_seq] = mtf(s_bwt)
    s_rleseq = rle(s_mtf_seq)
    [huff_d, s_bits] = huffman(s_rleseq)

    # decoding
    d_seq = rld(s_rleseq)
    d_bwt = mtf_inv([mtf_dict, d_seq])
    d_str = bwt_inv(d_bwt)

    print("BWT(" + sys.argv[1] + ") = »" + s_bwt + "«")
    print("MTF(BWT(" + sys.argv[1] + ")) = Dict: »" + mtf_dict + "«, Sequence: [ " + ''.join(str(e) + " " for e in s_mtf_seq) + "]")
    print("RLE([ " + ''.join(str(e) + " " for e in s_mtf_seq) + "]) = [ " + ''.join(str(e) + " " for e in s_rleseq) + "]")
    print("Huffman(" + s_bits + ")")

    #print("RLD([ " + ''.join(str(e) + " " for e in s_rleseq) + "]) = [ " + ''.join(str(e) + " " for e in d_seq) + "]")
    #print("MTF_inv( [ " + s_mtf_dict + ", [ " + ''.join(str(e) + " "  for e in d_seq) + "] ] ) = »" + d_bwt + "«")
    #print("BWT_inv(" + d_bwt + ") = »" + d_str + "«")


if __name__ == "__main__":
    main()
